var path = require('path');
var Builder = require('systemjs-builder');
var UglifyJS = require('uglify-js');
var fs = require('fs');

var builder = new Builder('./', 'systemjs.config.js');

builder
.bundle('app/app.component.js', 'bundle.js')
.then(function() {
    console.log('Bundling complete, minifying...');
    
    var bundle = UglifyJS.minify('bundle.js');
    
    fs.writeFile('bundle.min.js', bundle.code, function(err) {
        if(err) {
            console.log('Error minifying:');
            return console.log(err);
        }
        
        fs.unlink('bundle.js', function(err) {
            if(err) {
                console.log('Unable to delete temp bundle.js:')
                console.log(err);
            }
            
           console.log('Process complete!'); 
        });

        
    }); 
})
.catch(function(err) {
    console.log('Error bundling:');
    console.log(err);
});