import { Component } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';

import { GameComponent } from './game.component';
import { MainComponent } from './main.component';
import { ConfigurationService } from './configuration.service';

@Component({
	selector: 'my-app',
	templateUrl: 'app/app.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [ROUTER_PROVIDERS, ConfigurationService]
})
@RouteConfig([
{
	path: '/',
	name: 'Main',
	component: MainComponent,
	useAsDefault: true
},
{
	path: '/game',
	name: 'Game',
	component: GameComponent
}])
export class AppComponent {
	
	constructor(private router: Router, private config: ConfigurationService) {
	}
	
	gotoMain() {
		this.router.navigate(['Main']);
	}
}