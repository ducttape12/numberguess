import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router-deprecated';

import { CpuPlayer } from './players/cpu.player';
import { HumanPlayer } from './players/human.player';
import { IPlayer } from './players/iplayer.interface';
import { Game, Guess, GuessResult } from './game/game';
import { ConfigurationService } from './configuration.service';

@Component({
	selector: 'game',
	templateUrl: 'app/game.component.html',
	directives: [ROUTER_DIRECTIVES]
})
export class GameComponent {
    game: Game;
    userGuess: string;
    previousResult: Guess;
    showResults: boolean;
    name: string;
    index: number;
    lowBound: number;
    highBound: number;
    
    constructor(private config: ConfigurationService, private router: Router) {
        this.config.gameInProgress = true;
    }
    
    private getDisplayResultsDelay() {
        return 2500;
    }
    
    ngOnInit() {
        if(this.config.players.length < 2) {
		    this.router.navigate(['Main']);
        } else {
            this.game = new Game(this.config.players);
            this.initialPlayerMove();
        }
    }
    
    private initialPlayerMove() {
        // Make a snapshot of the current game state so it can be displayed to the user
        this.index = this.game.getCurrentPlayerIndex();
        this.name = this.game.getPlayers()[this.index].name;
        this.lowBound = this.game.getLowBound();
        this.highBound = this.game.getHighBound();
        
        this.previousResult = this.game.makeGuess();
        this.showResults = false;
        
        // Get user input
        if(this.previousResult === null) {
            this.userGuess = null;
            
        // Fake CPU thinking
        } else {
            this.displayResults();
        }
    }
    
    submitGuess() {
        this.previousResult = this.game.makeGuess(Number(this.userGuess));
        
        if(this.previousResult.result !== GuessResult.Invalid) {
            this.displayResults();
        }
    }
    
    displayResults() {
        this.showResults = true;
        
        if(this.previousResult.result !== GuessResult.Exact) {
            window.setTimeout(() => {
                this.initialPlayerMove();
            }, this.getDisplayResultsDelay());
        }
    }
    
    previousResultInvalid() {
        return this.previousResult !== null && this.previousResult.result === GuessResult.Invalid;
    }
    
    previousResultLow() {
        return this.previousResult !== null && this.previousResult.result === GuessResult.Low;
    }
    
    previousResultHigh() {
        return this.previousResult !== null && this.previousResult.result === GuessResult.High;
    }
    
    previousResultExact() {
        return this.previousResult !== null && this.previousResult.result === GuessResult.Exact;
    }
    
    showGame() {
        return typeof this.game !== 'undefined';
    }
}