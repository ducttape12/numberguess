import { IPlayer } from './iplayer.interface';

export class HumanPlayer implements IPlayer {
    name: string;
    
    constructor(name: string) {
        this.name = name;
    } 
    
    generateAnswer(low: number, high: number) {
        // Don't generate an answer
        return null;
    }
}