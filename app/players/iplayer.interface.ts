export interface IPlayer {
    name: string,
    
    generateAnswer(low: number, high: number): number;
}