import { IPlayer } from './iplayer.interface';

export class CpuPlayer implements IPlayer {
    name: string;
    
    constructor(name: string) {
        this.name = name;
    } 
    
    generateAnswer(low: number, high: number) {
        if(isNaN(low) || low === null) {
            throw new Error('low must be a number');
        }
        if(isNaN(high) || high === null) {
            throw new Error('high must be a number');
        }
        if(low < 0 || high < low) {
            throw new Error('low must be greater than 0, high must be greater than low');
        }
        
        // Generate an answer between low + 25% and high - 25%
        let quarter = Math.floor((high - low) / 4);
        let lowBound = low + quarter;
        let highBound = high - quarter;
        
        if(lowBound < low) { lowBound = low; }
        if(highBound > high) { highBound = high; }
        if(highBound < lowBound) {
            lowBound = low;
            highBound = high;
        }
        
        return Math.floor(Math.random() * (highBound - lowBound)) + lowBound;
    }
    
}