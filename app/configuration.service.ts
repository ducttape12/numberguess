import { Injectable } from '@angular/core';
import { IPlayer } from './players/iplayer.interface';

@Injectable()
export class ConfigurationService {
	players: Array<IPlayer>;
	gameInProgress: boolean;
	
	constructor() {
		this.players = [];
		this.gameInProgress = false;
	}
}