import { bootstrap } from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';

import { AppComponent } from './app.component';

// Step 3: Uncomment this line for production
//enableProdMode();
bootstrap(AppComponent);