import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router-deprecated';

import { CpuPlayer } from './players/cpu.player';
import { HumanPlayer } from './players/human.player';
import { IPlayer } from './players/iplayer.interface';
import { Game, Guess, GuessResult } from './game/game';
import { ConfigurationService } from './configuration.service';

const cpuNames = ['Gary', 'Patricia', 'Janice', 'Anthony', 'Roberto', 'Diane', 'Maria',
    'Christian', 'Cindy', 'Michael', 'Gregory', 'Michelle', 'Gail', 'Charlotte', 'Earl',
    'David', 'Jessica', 'Ken', 'Tara', 'Marilyn', 'Cory', 'Richard', 'Deborah', 'Thomas',
    'Marvin', 'Evelyn', 'Willie', 'Irma', 'Kristine', 'Bandit'];

class User {
    constructor(public isPlayer: boolean, public name: string) {}
}

@Component({
	selector: 'main',
	templateUrl: 'app/main.component.html',
})
export class MainComponent {
    users: Array<User>;
    
    constructor(private config: ConfigurationService, private router: Router) {
        this.config.gameInProgress = false;
    }
    
    ngOnInit() {
        this.users = [];
        
        for(let i = 0; i < this.config.players.length; i++) {
            this.users.push(new User(this.config.players[i] instanceof HumanPlayer, this.config.players[i].name))
        }
        
        // Default players
        if(this.users.length === 0) {
            this.addHuman();
            this.addCpu();
            this.addCpu();
        }
    }
    
    addHuman() {
        this.users.push(new User(true, ''));
    }
    
    addCpu() {
        let randomName = cpuNames[ Math.floor(Math.random() * cpuNames.length) ];
        
        this.users.push(new User(false, randomName));
    }
    
    removeUser(i) {
        this.users.splice(i, 1);
    }
    
    startGame() {
        if(this.users.length < 2) { return; }
        
        let players: Array<IPlayer> = [];
        for(let i = 0; i < this.users.length; i++) {
            
            // Apply player names if they're missing
            if(this.users[i].name.trim().length === 0) {
                this.users[i].name = 'Player ' + (i + 1);
            }
            
            if(this.users[i].isPlayer) {
                players.push(new HumanPlayer(this.users[i].name));
            } else {
                players.push(new CpuPlayer(this.users[i].name));
            }
        }
       
        this.config.players = players;
        
		this.router.navigate(['Game']);
    }
}