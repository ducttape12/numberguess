<h1>Number Guess</h1>
<p>Try to guess the secret number before your opponents do!</p>
<p>Originally written as a learning project to learn Angular 2 and TypeScript.</p>

<h2>How to Play</h2>
<p>On the main screen, add any number of human and CPU players.  Once you start the game, players take turns guessing the hidden number (1 - 999).  
If a guess is too low or too high, that will become the new lower or upper bound respectively.  Play continues until a player correctly guesses the number.</p>

<h2><a href="http://www.keithott.com/software/numberguess">Play in your browser now on KeithOtt.com!</a></h2>

<p>Want more games and apps?  Check out <a href="www.keithott.com">KeithOtt.com</a> for more!</p>

<h2>How to Run for Development</h2>
<p>Install node.js and clone the git repo.  Open a command prompt in the root of the project and run the following command:</p>

<pre>npm install</pre>

<p>Start the web server by running the following command:</p>
<pre>npm start</pre>

<h2>Running the Tests</h2>
<p>With "npm start" still running, navigate to <a href="http://localhost:3000/test.html">http://localhost:3000/test.html</a>.  As changes are made to the tests, the
source will be automatically recompiled and the browser will auto reload.</p>  

<h2>How to Deploy to Production</h2>
<p>I'll admit, my build system could use some work, so there's a few manual steps you'll need to do:</p>
<ol>
    <li>Open index.html and look for "Step 1".  Modify the base URL to match what it will be in prod.  (For example, if it'll live at example.com/cokes, change the value to href="/cokes/".)</li>
    <li>Still in index.html, find "Step 2" and uncomment the following line.</li>
    <li>Open "main.ts" and look for "Step 3".  Uncomment the "enableProdMode()" line.</li>
    <li>Run "npm start" once more to recompile the TypeScript.</li>
    <li>Run "node build.js"</li>
</ol>

<p>Not all files are included in the bundle, so to deploy you'll want to copy everything to your webserver (yeah yeah, I know).</p>

<h1>License</h1>
<p>Copyright 2016 Keith Ott</p>

<p>Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at</p>

<p><a href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</a></p>

<p>Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.</p>