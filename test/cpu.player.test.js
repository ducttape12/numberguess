"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tsUnit = require('tsunit.external/tsUnit');
var cpu_player_1 = require('../app/players/cpu.player');
var sharedCpu = new cpu_player_1.CpuPlayer('Test');
var CpuPlayerTests = (function (_super) {
    __extends(CpuPlayerTests, _super);
    function CpuPlayerTests() {
        _super.apply(this, arguments);
    }
    CpuPlayerTests.prototype.setNameWithConstructor = function () {
        this.areIdentical('Test', sharedCpu.name);
    };
    CpuPlayerTests.prototype.changeName = function () {
        var cpu = new cpu_player_1.CpuPlayer('Test');
        cpu.name = 'Test2';
        this.areIdentical('Test2', cpu.name);
    };
    CpuPlayerTests.prototype.generateAnswerWithNullLow = function () {
        try {
            var answer = sharedCpu.generateAnswer(null, null);
            this.fail('null low parameter didn\'t throw an exception');
        }
        catch (err) {
            this.areIdentical('low must be a number', err.message);
        }
    };
    CpuPlayerTests.prototype.generateAnswerWithNullHigh = function () {
        try {
            var answer = sharedCpu.generateAnswer(5, null);
            this.fail('null high parameter didn\'t throw an exception');
        }
        catch (err) {
            this.areIdentical('high must be a number', err.message);
        }
    };
    CpuPlayerTests.prototype.generateAnswerWithLowBelowZero = function () {
        try {
            var answer = sharedCpu.generateAnswer(-1, 5);
            this.fail('low less than 0 should throw an exception');
        }
        catch (err) {
            this.areIdentical('low must be greater than 0, high must be greater than low', err.message);
        }
    };
    CpuPlayerTests.prototype.generateAnswerWithHighLessThanLow = function () {
        try {
            var answer = sharedCpu.generateAnswer(5, 2);
            this.fail('high less than low should throw an exception');
        }
        catch (err) {
            this.areIdentical('low must be greater than 0, high must be greater than low', err.message);
        }
    };
    CpuPlayerTests.prototype.generateAnswer = function () {
        var answer = sharedCpu.generateAnswer(0, 100);
        this.isTrue(answer >= 25 && answer <= 75);
    };
    CpuPlayerTests.prototype.generateAnswerRounding = function () {
        var answer = sharedCpu.generateAnswer(5, 6);
        this.isTrue(answer >= 5 && answer <= 6);
    };
    return CpuPlayerTests;
}(tsUnit.TestClass));
exports.CpuPlayerTests = CpuPlayerTests;
//# sourceMappingURL=cpu.player.test.js.map