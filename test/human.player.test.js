"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tsUnit = require('tsunit.external/tsUnit');
var human_player_1 = require('../app/players/human.player');
var HumanPlayerTests = (function (_super) {
    __extends(HumanPlayerTests, _super);
    function HumanPlayerTests() {
        _super.apply(this, arguments);
    }
    HumanPlayerTests.prototype.setNameWithConstructor = function () {
        var human = new human_player_1.HumanPlayer('Test');
        this.areIdentical('Test', human.name);
    };
    HumanPlayerTests.prototype.changeName = function () {
        var human = new human_player_1.HumanPlayer('Test');
        human.name = 'Test2';
        this.areIdentical('Test2', human.name);
    };
    HumanPlayerTests.prototype.generateAnswerReturnsNull = function () {
        var human = new human_player_1.HumanPlayer('Test');
        var answer = human.generateAnswer(null, null);
        this.areIdentical(null, answer);
    };
    return HumanPlayerTests;
}(tsUnit.TestClass));
exports.HumanPlayerTests = HumanPlayerTests;
//# sourceMappingURL=human.player.test.js.map