import * as tsUnit from 'tsunit.external/tsUnit';

import * as CpuTests from './cpu.player.test';
import * as HumanTests from './human.player.test';
import * as GameTests from './game.test';

var test = new tsUnit.Test(CpuTests, HumanTests, GameTests).run().showResults('result');