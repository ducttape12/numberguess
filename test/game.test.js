"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tsUnit = require('tsunit.external/tsUnit');
var human_player_1 = require('../app/players/human.player');
var cpu_player_1 = require('../app/players/cpu.player');
var game_1 = require('../app/game/game');
var humanPlayers = [
    new human_player_1.HumanPlayer('Human 1'),
    new human_player_1.HumanPlayer('Human 2'),
    new human_player_1.HumanPlayer('Human 3')
];
var cpuPlayers = [
    new cpu_player_1.CpuPlayer('Cpu 1'),
    new cpu_player_1.CpuPlayer('Cpu 2'),
    new cpu_player_1.CpuPlayer('Cpu 3')
];
var GameTests = (function (_super) {
    __extends(GameTests, _super);
    function GameTests() {
        _super.apply(this, arguments);
    }
    GameTests.prototype.nullPlayersSpecified = function () {
        try {
            var game = new game_1.Game(null);
            this.fail('Should not be able to create game with null players');
        }
        catch (err) {
            this.areIdentical('players array must have one or more players', err.message);
        }
    };
    GameTests.prototype.noPlayersSpecified = function () {
        try {
            var game = new game_1.Game(new Array());
            this.fail('Should not be able to create game with no players');
        }
        catch (err) {
            this.areIdentical('players array must have one or more players', err.message);
        }
    };
    GameTests.prototype.initializeGame = function () {
        var game = new game_1.Game(humanPlayers);
        this.areIdentical(1, game.getLowBound());
        this.areIdentical(999, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
        this.areIdentical(0, game.getCurrentPlayerIndex());
        this.isFalse(isNaN(game.getWinningNumber()));
    };
    GameTests.prototype.initializeGameWithWinningNumber = function () {
        var winningNumber = 50;
        var game = new game_1.Game(humanPlayers, winningNumber);
        this.areIdentical(1, game.getLowBound());
        this.areIdentical(999, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
        this.areIdentical(0, game.getCurrentPlayerIndex());
        this.areIdentical(winningNumber, game.getWinningNumber());
    };
    GameTests.prototype.initialGuessWithHuman = function () {
        var game = new game_1.Game(humanPlayers);
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess();
        this.areIdentical(null, result);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.guessOutsideLowBound = function () {
        var game = new game_1.Game(humanPlayers);
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var guess = low - 1;
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.Invalid, result.result);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(guess, result.guess);
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.guessOutsideHighBound = function () {
        var game = new game_1.Game(humanPlayers);
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var guess = high + 1;
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.Invalid, result.result);
        this.areIdentical(guess, result.guess);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.lowGuess = function () {
        var game = new game_1.Game(humanPlayers, 50);
        var guess = 45;
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.Low, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(guess, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.guessLowBounds = function () {
        var game = new game_1.Game(humanPlayers, 50);
        var guess = game.getLowBound();
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.Low, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(guess, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.highGuess = function () {
        var game = new game_1.Game(humanPlayers, 50);
        var guess = 55;
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.High, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(guess, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.guessHighBounds = function () {
        var game = new game_1.Game(humanPlayers, 50);
        var guess = game.getHighBound();
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.High, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(guess, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.nullGuess = function () {
        var game = new game_1.Game(humanPlayers, 50);
        var guess = null;
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess(guess);
        this.areIdentical(game_1.GuessResult.Invalid, result.result);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    };
    GameTests.prototype.exactGuess = function () {
        var game = new game_1.Game(humanPlayers);
        var winningNumber = game.getWinningNumber();
        var current = game.getCurrentPlayerIndex();
        var low = game.getLowBound();
        var high = game.getHighBound();
        var result = game.makeGuess(winningNumber);
        this.areIdentical(game_1.GuessResult.Exact, result.result);
        this.areIdentical(winningNumber, result.guess);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(true, game.isGameOver());
    };
    GameTests.prototype.cannotGuessAfterGameOver = function () {
        var game = new game_1.Game(humanPlayers);
        var winningNumber = game.getWinningNumber();
        var currentPlayer = game.getCurrentPlayerIndex();
        game.makeGuess(winningNumber);
        var result = game.makeGuess(winningNumber);
        this.areIdentical(game_1.GuessResult.Invalid, result.result);
        this.areIdentical(winningNumber, result.guess);
        this.areIdentical(currentPlayer, game.getCurrentPlayerIndex());
    };
    GameTests.prototype.cpuGuesses = function () {
        var game = new game_1.Game(cpuPlayers);
        var currentPlayer = game.getCurrentPlayerIndex();
        var result = game.makeGuess();
        this.areNotIdentical(game_1.GuessResult.Invalid, result.result);
        this.areIdentical(currentPlayer + 1, game.getCurrentPlayerIndex());
    };
    GameTests.prototype.playersLoop = function () {
        var game = new game_1.Game(humanPlayers, 50);
        game.makeGuess(1); // Player 1
        game.makeGuess(2); // Player 2
        game.makeGuess(3); // Player 3
        this.areIdentical(0, game.getCurrentPlayerIndex());
    };
    return GameTests;
}(tsUnit.TestClass));
exports.GameTests = GameTests;
//# sourceMappingURL=game.test.js.map