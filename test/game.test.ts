import * as tsUnit from 'tsunit.external/tsUnit';

import { HumanPlayer } from '../app/players/human.player';
import { CpuPlayer } from '../app/players/cpu.player';
import { IPlayer } from '../app/players/iplayer.interface';
import { Game, GuessResult, Guess } from '../app/game/game';


const humanPlayers: Array<IPlayer> = [
    new HumanPlayer('Human 1'),
    new HumanPlayer('Human 2'),
    new HumanPlayer('Human 3')
];

const cpuPlayers: Array<IPlayer> = [
    new CpuPlayer('Cpu 1'),
    new CpuPlayer('Cpu 2'),
    new CpuPlayer('Cpu 3')
]; 

export class GameTests extends tsUnit.TestClass {
    nullPlayersSpecified() {
        try {
            let game = new Game(null);
            this.fail('Should not be able to create game with null players');
        } catch(err) {
            this.areIdentical('players array must have one or more players', err.message);
        }
    }
    
    noPlayersSpecified() {
        try {
            let game = new Game(new Array<IPlayer>());
            this.fail('Should not be able to create game with no players');
        } catch(err) {
            this.areIdentical('players array must have one or more players', err.message);
        }
    }
    
    initializeGame() {
        let game = new Game(humanPlayers);
        this.areIdentical(1, game.getLowBound());
        this.areIdentical(999, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
        this.areIdentical(0, game.getCurrentPlayerIndex());
        this.isFalse(isNaN(game.getWinningNumber()));
    }
    
    initializeGameWithWinningNumber() {
        const winningNumber = 50;
        let game = new Game(humanPlayers, winningNumber);
        this.areIdentical(1, game.getLowBound());
        this.areIdentical(999, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
        this.areIdentical(0, game.getCurrentPlayerIndex());
        this.areIdentical(winningNumber, game.getWinningNumber());
    }
    
    initialGuessWithHuman() {
        let game = new Game(humanPlayers);
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess();
        
        this.areIdentical(null, result);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    guessOutsideLowBound() {
        let game = new Game(humanPlayers);
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        const guess = low - 1;
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.Invalid, result.result);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(guess, result.guess);
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    guessOutsideHighBound() {
        let game = new Game(humanPlayers);
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        const guess = high + 1;
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.Invalid, result.result);
        this.areIdentical(guess, result.guess);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    lowGuess() {
        let game = new Game(humanPlayers, 50);
        const guess = 45;
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.Low, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(guess, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    guessLowBounds() {
        let game = new Game(humanPlayers, 50);
        let guess = game.getLowBound();
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.Low, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(guess, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    highGuess() {
        let game = new Game(humanPlayers, 50);
        const guess = 55;
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.High, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(guess, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    guessHighBounds() {
        let game = new Game(humanPlayers, 50);
        let guess = game.getHighBound();
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.High, result.result);
        this.areIdentical(current + 1, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(guess, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    nullGuess() {
        let game = new Game(humanPlayers, 50);
        const guess: number = null;
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess(guess);
        
        this.areIdentical(GuessResult.Invalid, result.result);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(low, game.getLowBound());
        this.areIdentical(high, game.getHighBound());
        this.areIdentical(false, game.isGameOver());
    }
    
    exactGuess() {
        let game = new Game(humanPlayers);
        let winningNumber = game.getWinningNumber();
        let current = game.getCurrentPlayerIndex();
        let low = game.getLowBound();
        let high = game.getHighBound();
        let result = game.makeGuess(winningNumber);
        
        this.areIdentical(GuessResult.Exact, result.result);
        this.areIdentical(winningNumber, result.guess);
        this.areIdentical(current, game.getCurrentPlayerIndex());
        this.areIdentical(true, game.isGameOver());
    }
    
    cannotGuessAfterGameOver() {
        let game = new Game(humanPlayers);
        let winningNumber = game.getWinningNumber();
        let currentPlayer = game.getCurrentPlayerIndex();
        game.makeGuess(winningNumber);
        let result = game.makeGuess(winningNumber);
        
        this.areIdentical(GuessResult.Invalid, result.result);
        this.areIdentical(winningNumber, result.guess);
        this.areIdentical(currentPlayer, game.getCurrentPlayerIndex());
    }
    
    cpuGuesses() {
        let game = new Game(cpuPlayers);
        let currentPlayer = game.getCurrentPlayerIndex();
        let result = game.makeGuess();
        
        this.areNotIdentical(GuessResult.Invalid, result.result);
        this.areIdentical(currentPlayer + 1, game.getCurrentPlayerIndex());
    }
    
    playersLoop() {
        let game = new Game(humanPlayers, 50);
        game.makeGuess(1); // Player 1
        game.makeGuess(2); // Player 2
        game.makeGuess(3); // Player 3
        
        this.areIdentical(0, game.getCurrentPlayerIndex());
    }
}