import * as tsUnit from 'tsunit.external/tsUnit';

import { CpuPlayer } from '../app/players/cpu.player';

const sharedCpu = new CpuPlayer('Test');

export class CpuPlayerTests extends tsUnit.TestClass {
    setNameWithConstructor() {
        this.areIdentical('Test', sharedCpu.name);
    }
    
    changeName() {
        let cpu = new CpuPlayer('Test');
        cpu.name = 'Test2';
        this.areIdentical('Test2', cpu.name);
    }
    
    generateAnswerWithNullLow() {
        try {
            let answer = sharedCpu.generateAnswer(null, null);
            this.fail('null low parameter didn\'t throw an exception');
        } catch(err) {
            this.areIdentical('low must be a number', err.message);
        }
    }
    
    generateAnswerWithNullHigh() {
        try {
            let answer = sharedCpu.generateAnswer(5, null);
            this.fail('null high parameter didn\'t throw an exception');
        } catch(err) {
            this.areIdentical('high must be a number', err.message);
        }
    }
    
    generateAnswerWithLowBelowZero() {
        try {
            let answer = sharedCpu.generateAnswer(-1, 5);
            this.fail('low less than 0 should throw an exception');
        } catch(err) {
            this.areIdentical('low must be greater than 0, high must be greater than low', err.message);
        }
    }
    
    generateAnswerWithHighLessThanLow() {
        try {
            let answer = sharedCpu.generateAnswer(5, 2);
            this.fail('high less than low should throw an exception');
        } catch(err) {
            this.areIdentical('low must be greater than 0, high must be greater than low', err.message);
        }
    }
    
    generateAnswer() {
        let answer = sharedCpu.generateAnswer(0, 100);
        this.isTrue(answer >= 25 && answer <= 75);
    }
    
    generateAnswerRounding() {
        let answer = sharedCpu.generateAnswer(5, 6);
        this.isTrue(answer >= 5 && answer <= 6);
    }
}