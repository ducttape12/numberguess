import * as tsUnit from 'tsunit.external/tsUnit';

import { HumanPlayer } from '../app/players/human.player';

export class HumanPlayerTests extends tsUnit.TestClass {
    setNameWithConstructor() {
        let human = new HumanPlayer('Test');
        this.areIdentical('Test', human.name);
    }
    
    changeName() {
        let human = new HumanPlayer('Test');
        human.name = 'Test2';
        this.areIdentical('Test2', human.name);
    }
    
    generateAnswerReturnsNull() {
        let human = new HumanPlayer('Test');
        let answer = human.generateAnswer(null, null);
        
        this.areIdentical(null, answer);
    }
}